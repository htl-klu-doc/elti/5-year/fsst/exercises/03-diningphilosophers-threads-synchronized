package at.htlklu.fsst;

public class Philosoph extends Thread {
	private String name;
	private Fork left;
	private Fork right;
	private int delayEating = 1000;
	private int delayThinking = 300;
	private boolean stillHungry = true;
	private int numberOfMeals = 0;

	public Philosoph(String name, Fork left, Fork right) {
		super();
		this.name = name;
		this.left = left;
		this.right = right;
	}

	public String getPhilosophName() {
		return name;
	}

	@Override
	public void run() {
		while (stillHungry) {
			thinking();
			if (left.take(this)) {
				if (right.take(this)) {
					eating();
					right.giveBack(this);
				}

				left.giveBack(this);
			}
		}

		System.out.printf("%25s finished eating & thinking%n", this.getPhilosophName());
	}

	public void thinking() {
		try {
			Thread.sleep(delayThinking);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.err.println("Philosoph already hungry!!!");
		}
	}

	public void eating() {
		numberOfMeals++;

		if (numberOfMeals >= 5)
			stillHungry = false;
		try {
			Thread.sleep(delayEating);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.err.println("Philosoph wants to start thinking again!");
		}
	}
}
