package at.htlklu.fsst;

public class Fork {
	String name;
	boolean currentlyUsed = false;

	public Fork(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Fork: %3s", this.name);
	}

	public boolean take(Philosoph philosoph) {
		if (this.currentlyUsed) {
			System.out.printf("%s is currently in use!%n", this.toString());
			return false;
		}
		
		this.currentlyUsed = true;
		System.out.printf("%25s <- %s%n", philosoph.getPhilosophName(), this.toString());
		return true;
	}

	public void giveBack(Philosoph philosoph) {
		this.currentlyUsed = false;
		System.out.printf("%25s -> %s%n", philosoph.getPhilosophName(), this.toString());
	}
}
