package at.htlklu.fsst;

import java.util.ArrayList;

public class Tisch {
	public static void main(String[] args) {
		ArrayList<Philosoph> philosophs = new ArrayList<>();
		ArrayList<Fork> forks = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			forks.add(new Fork(String.format("%02d", i)));
		}

		philosophs.add(new Philosoph("Friedrich Nietzsche", forks.get(0), forks.get(1)));
		philosophs.add(new Philosoph("Karl Marx", forks.get(1), forks.get(2)));
		philosophs.add(new Philosoph("René Descartes", forks.get(2), forks.get(3)));
		philosophs.add(new Philosoph("Galileo Galilei", forks.get(3), forks.get(4)));
		philosophs.add(new Philosoph("Friedrich Engels", forks.get(4), forks.get(5)));

		long start = System.currentTimeMillis();
		for (Philosoph philosoph : philosophs) {
			philosoph.start();
		}
		for (Philosoph philosoph : philosophs) {
			try {
				philosoph.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long end = System.currentTimeMillis();
		System.out.printf("Took %05d ms and everyone has had 5 meals :)%n", end - start);
	}
}
